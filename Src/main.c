
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * This notice applies to any and all portions of this file
  * that are not between comment pairs USER CODE BEGIN and
  * USER CODE END. Other portions of this file, whether 
  * inserted by the user or by software development tools
  * are owned by their respective copyright owners.
  *
  * Copyright (c) 2018 STMicroelectronics International N.V. 
  * All rights reserved.
  *
  * Redistribution and use in source and binary forms, with or without 
  * modification, are permitted, provided that the following conditions are met:
  *
  * 1. Redistribution of source code must retain the above copyright notice, 
  *    this list of conditions and the following disclaimer.
  * 2. Redistributions in binary form must reproduce the above copyright notice,
  *    this list of conditions and the following disclaimer in the documentation
  *    and/or other materials provided with the distribution.
  * 3. Neither the name of STMicroelectronics nor the names of other 
  *    contributors to this software may be used to endorse or promote products 
  *    derived from this software without specific written permission.
  * 4. This software, including modifications and/or derivative works of this 
  *    software, must execute solely and exclusively on microcontroller or
  *    microprocessor devices manufactured by or for STMicroelectronics.
  * 5. Redistribution and use of this software other than as permitted under 
  *    this license is void and will automatically terminate your rights under 
  *    this license. 
  *
  * THIS SOFTWARE IS PROVIDED BY STMICROELECTRONICS AND CONTRIBUTORS "AS IS" 
  * AND ANY EXPRESS, IMPLIED OR STATUTORY WARRANTIES, INCLUDING, BUT NOT 
  * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
  * PARTICULAR PURPOSE AND NON-INFRINGEMENT OF THIRD PARTY INTELLECTUAL PROPERTY
  * RIGHTS ARE DISCLAIMED TO THE FULLEST EXTENT PERMITTED BY LAW. IN NO EVENT 
  * SHALL STMICROELECTRONICS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
  * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
  * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, 
  * OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING 
  * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
  * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "stm32l4xx_hal.h"
#include "dma.h"
#include "fatfs.h"
#include "sdmmc.h"
#include "tim.h"
#include "usart.h"
#include "gpio.h"

/* USER CODE BEGIN Includes */
#include "SD.h"
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "string.h"
#include "ff.h"
/* USER CODE END Includes */

/* Private variables ---------------------------------------------------------*/

/* USER CODE BEGIN PV */

#define RX_SIZE 250
#define MEM_PACKET_SIZE 113 // bno 54with tick //ST mems is 113
uint8_t RX_data[RX_SIZE];
uint16_t RX_size = RX_SIZE;

#define RX_UART1_SIZE 400
uint8_t RxUart1[RX_UART1_SIZE];

#define GPS_PACKET_SIZE 188

typedef struct
{
  uint32_t Len;
  char* Data;
} TMsg;

char TMsgData[250];
TMsg msg = {.Data =&TMsgData[0], .Len=0};


typedef enum {
	BUFF_READY,
	BUFF_FULL,
	BUFF_OVERRUN
}buffStatus;

typedef enum sdLogState{
	LOG_START,
	LOG_STOP,
	LOG_ERROR,
	LOG_ACTIVE,
	LOG_ERROR_WRTIE,
	LOG_ERROR_OPEN,
	LOG_ERROR_DMA,
	LOG_OFF
}sdLogState;

typedef enum sdInitStatus{
	SD_OK,
	SD_F_ERROR,
	SD_BSP_ERROR,
	SD_HAL_ERROR
}SDRESULT;

typedef enum LD2_State{
LD2_ON,
LD2_OFF,
LD2_FLASH_1,
LD2_FLASH_2,
LD2_FLASH_3
}LD2State_t;


typedef struct hLogger{
	BYTE   			drv;
	DWORD    		lba;
	uint16_t 		sectorsRemain;
	sdLogState 		LogState;
	FIL 			* file;
	uint8_t 		buffLogger[2][512];
	uint8_t 		buffActive; //index to Store Data
	uint16_t 		buffCursorLocation;  //max 512
	buffStatus 		buffWriteStatus; //set when SD write complete
	uint32_t 		logStartTime;
	uint8_t			err;
}hLogger;

hLogger hLog ={
		.sectorsRemain=0,
		.LogState=LOG_OFF,
		.buffActive=0,
		.buffCursorLocation=0,
		.buffWriteStatus=BUFF_READY,
		.err=0};

/* OTHER */


/* Private variables ---------------------------------------------------------*/

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);

/* USER CODE BEGIN PFP */
/* Private function prototypes -----------------------------------------------*/
void User_Common_SendData(TMsg msg);
void User_Set_LED_State(LD2State_t LD2);
HAL_StatusTypeDef  User_Start_DMA(UART_HandleTypeDef *huart);

SDRESULT User_SD_Init(void);
SDRESULT User_SD_MEM_LogData(void);
FRESULT User_Buffer_Check(uint8_t forceWrite);
buffStatus User_Buffer_Pack(void);

/* USER CODE END PFP */

/* USER CODE BEGIN 0 */
uint8_t BufferData[512];

typedef struct BufferT{
	uint8_t * data;
	uint16_t bytesToWrite;
	uint8_t busy;
}buffer_t;

buffer_t buffData ={.data = &BufferData[0], .bytesToWrite = 0, .busy = 0};


/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  *
  * @retval None
  */
int main(void)
{
  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* MCU Configuration----------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_DMA_Init();
  MX_SDMMC1_SD_Init();
  MX_USART2_UART_Init();
  MX_FATFS_Init();
  MX_TIM1_Init();
  MX_USART3_UART_Init();
  MX_TIM2_Init();
  MX_USART1_UART_Init();
  /* USER CODE BEGIN 2 */


	SDRESULT sdr = User_SD_Init();
	if(sdr != SD_OK){
		msg.Len = sprintf(msg.Data, "%s","SD_FAIL\n" );
		User_Common_SendData(msg);
		User_Set_LED_State(LD2_FLASH_3);
		while(1);
	}

	/*litte endian test*/
    uint32_t i=0x01234567;
    // return 0 for big endian, 1 for little endian.
    if((*((uint8_t*)(&i))) == 0x67){
    	msg.Len = sprintf(msg.Data, "%s","little endian machine\n" );
    	User_Common_SendData(msg);
    }


  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
	while (1)
	{
  /* USER CODE END WHILE */

  /* USER CODE BEGIN 3 */

	User_SD_MEM_LogData();

	if(hLog.buffWriteStatus == BUFF_FULL)
		User_Buffer_Check(0);

	#ifdef USE_OLD
		if(buffData.bytesToWrite > 0)
		 User_Buffer_Pack();
	#endif


	}
  /* USER CODE END 3 */

}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{

  RCC_OscInitTypeDef RCC_OscInitStruct;
  RCC_ClkInitTypeDef RCC_ClkInitStruct;
  RCC_PeriphCLKInitTypeDef PeriphClkInit;

    /**Initializes the CPU, AHB and APB busses clocks 
    */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.HSICalibrationValue = 16;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSI;
  RCC_OscInitStruct.PLL.PLLM = 1;
  RCC_OscInitStruct.PLL.PLLN = 10;
  RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV7;
  RCC_OscInitStruct.PLL.PLLQ = RCC_PLLQ_DIV2;
  RCC_OscInitStruct.PLL.PLLR = RCC_PLLR_DIV2;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

    /**Initializes the CPU, AHB and APB busses clocks 
    */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_4) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

  PeriphClkInit.PeriphClockSelection = RCC_PERIPHCLK_USART1|RCC_PERIPHCLK_USART2
                              |RCC_PERIPHCLK_USART3|RCC_PERIPHCLK_SDMMC1;
  PeriphClkInit.Usart1ClockSelection = RCC_USART1CLKSOURCE_PCLK2;
  PeriphClkInit.Usart2ClockSelection = RCC_USART2CLKSOURCE_PCLK1;
  PeriphClkInit.Usart3ClockSelection = RCC_USART3CLKSOURCE_PCLK1;
  PeriphClkInit.Sdmmc1ClockSelection = RCC_SDMMC1CLKSOURCE_PLLSAI1;
  PeriphClkInit.PLLSAI1.PLLSAI1Source = RCC_PLLSOURCE_HSI;
  PeriphClkInit.PLLSAI1.PLLSAI1M = 1;
  PeriphClkInit.PLLSAI1.PLLSAI1N = 10;
  PeriphClkInit.PLLSAI1.PLLSAI1P = RCC_PLLP_DIV7;
  PeriphClkInit.PLLSAI1.PLLSAI1Q = RCC_PLLQ_DIV4;
  PeriphClkInit.PLLSAI1.PLLSAI1R = RCC_PLLR_DIV2;
  PeriphClkInit.PLLSAI1.PLLSAI1ClockOut = RCC_PLLSAI1_48M2CLK;
  if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInit) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

    /**Configure the main internal regulator output voltage 
    */
  if (HAL_PWREx_ControlVoltageScaling(PWR_REGULATOR_VOLTAGE_SCALE1) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

    /**Configure the Systick interrupt time 
    */
  HAL_SYSTICK_Config(HAL_RCC_GetHCLKFreq()/1000);

    /**Configure the Systick 
    */
  HAL_SYSTICK_CLKSourceConfig(SYSTICK_CLKSOURCE_HCLK);

  /* SysTick_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(SysTick_IRQn, 0, 0);
}

/* USER CODE BEGIN 4 */

void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin){
	static uint8_t PB_counter =0 ; 			//push button counter for cycle EXTI
	uint32_t 	IntTimeNow;
	static uint32_t IntTimeLastPIN13 = 0;

	//set the time now for software debounce
	IntTimeNow = HAL_GetTick();


	if (GPIO_Pin == GPIO_PIN_13){
		/* Manage software debouncing*/
		/* If I receive a button interrupt after more than 300 ms from the first one I get it, otherwise I discard it */
		if ((IntTimeNow - IntTimeLastPIN13) > 300U)
		{
			IntTimeLastPIN13 = IntTimeNow;  //reset the last event time
			switch (PB_counter) {
				case 0:
					PB_counter ++;
					if(hLog.LogState == LOG_ACTIVE)	break;
					hLog.LogState = LOG_START;
					break;

				case 1:
					PB_counter = 0;
					hLog.LogState = LOG_STOP;
					break;

				default:
					break;
			}
		}
	 }

	if (GPIO_Pin == GPIO_PIN_5){
		HAL_SD_MspDeInit(&hsd1);
	}
}


/*Send message to... what ever decided here. UART2*/
void User_Common_SendData(TMsg msg){

	HAL_UART_Transmit(&huart2, (uint8_t*)msg.Data, msg.Len,1000);

}


SDRESULT User_SD_Init(void){
	SDRESULT sdr = SD_OK;

	/** Setup SD */
	MSDRESULT mr = BSP_SD_Init();
	if(mr != BSP_MSD_OK) return SD_BSP_ERROR;

	/* mount the drive initialized*/
	FRESULT fr = f_mount(&SDFatFS, (TCHAR const*)SDPath, 0);
	if(fr != FR_OK) return SD_F_ERROR;


 	//write some stuff to a file, just for testing
	fr = SD_File_Write("UartLog.txt", (uint8_t*)"Verion1\n\0");
	if(fr != FR_OK){
	  //turn off SD
	  return fr;
	}


	return sdr;
}

void User_Set_LED_State(LD2State_t LD2){
	switch (LD2) {
		case LD2_OFF:
			HAL_TIM_PWM_Stop(&htim2,TIM_CHANNEL_1);
			break;
		case LD2_ON:
			__HAL_TIM_SET_PRESCALER(&htim2,500);
			__HAL_TIM_SET_COMPARE(&htim2,TIM_CHANNEL_1,1000);
			HAL_TIM_PWM_Start(&htim2,TIM_CHANNEL_1);
			break;
		case LD2_FLASH_1:
			__HAL_TIM_SET_PRESCALER(&htim2,50);
			__HAL_TIM_SET_COMPARE(&htim2,TIM_CHANNEL_1,5000);
			HAL_TIM_PWM_Start(&htim2,TIM_CHANNEL_1);
			break;
		case LD2_FLASH_2:
			__HAL_TIM_SET_PRESCALER(&htim2,500);
			__HAL_TIM_SET_COMPARE(&htim2,TIM_CHANNEL_1,5000);
			HAL_TIM_PWM_Start(&htim2,TIM_CHANNEL_1);
			break;
		case LD2_FLASH_3:
			__HAL_TIM_SET_PRESCALER(&htim2,5000);
			__HAL_TIM_SET_COMPARE(&htim2,TIM_CHANNEL_1,5000);
			HAL_TIM_PWM_Start(&htim2,TIM_CHANNEL_1);
			break;

		default:
			break;
	}
}

SDRESULT User_SD_MEM_LogData(void){

	FRESULT fr = FR_OK;
	SDRESULT sdr = SD_OK;

	switch (hLog.LogState) {
	/*LOG_START, LOG_STOP, LOG_ERROR, LOG_ACTIVE, LOG_ERROR_WRTIE, LOG_ERROR_OPEN, LOG_OFF*/
		case LOG_ERROR:
		//need to restart everything
			fr = f_close(&SDFile);
			HAL_UART_DMAStop(&huart3);
			User_Set_LED_State(LD2_FLASH_1);
			hLog.LogState = LOG_OFF;
			break;
		case LOG_ERROR_DMA:
		//need to restart everything
			HAL_UART_DMAStop(&huart3);
			hLog.LogState = LOG_ERROR;
			break;

		case LOG_ERROR_OPEN:
			msg.Len = sprintf(msg.Data, "%s", "File Open Error");
			User_Common_SendData(msg);

			fr = f_close(&SDFile);
			User_Set_LED_State(LD2_FLASH_2);
			hLog.LogState = LOG_OFF;
			break;
		case LOG_ERROR_WRTIE:
			msg.Len = sprintf(msg.Data, "%s", "File Write Error");
			User_Common_SendData(msg);

			fr = f_close(&SDFile);
			User_Set_LED_State(LD2_FLASH_3);
			hLog.LogState = LOG_OFF;
			break;

		case LOG_START:
			//Turn on the SD
			//Initialize the SD, bit over overkill, but have the time
			HAL_GPIO_WritePin(GPIOC,GPIO_PIN_6,GPIO_PIN_SET);
			sdr = User_SD_Init();
			if(sdr!=SD_OK){
				hLog.LogState = LOG_ERROR_OPEN;
				return sdr;
			}

			char fn[20];
			const char *fname = &fn[0];

			FILINFO fno;
			static uint16_t NextFileNumber = 0;
			do {
				/*create a file name*/
				sprintf(&fn[0], "%s%i.dat","LOG_",NextFileNumber++);
				/*find file, check exists, index if it does*/
				fr = f_stat (fname, &fno);

			} while (fr == FR_OK && fr !=FR_NO_FILE && NextFileNumber<99); //*consider adding timeout

			if(fr!=FR_NO_FILE){
				/*error or all index taken*/
				NextFileNumber =0;
		    	hLog.LogState = LOG_ERROR_OPEN;
		    	return SD_F_ERROR;
			}

		    /* Creating a new contiguous file */
		    fr = f_open(&SDFile, fname, FA_WRITE|FA_CREATE_ALWAYS);
		    if (fr != FR_OK) { /* Check if the file has been opened */
		        //die("Failed to open the file.");
		    	hLog.LogState = LOG_ERROR_OPEN;
		    	return SD_F_ERROR;
		    }

		    /* Alloacte a 1 MiB of contiguous area to the file */
		    //fr = f_expand(&SDFile, 10485760, 1); // 20480 sectors
		    fr = f_expand(&SDFile, 5242880, 1); //10240 sectors

		    if (fr!=FR_OK) { /* Check if the file has been expanded */
		        f_close(&SDFile);
		        //die("Failed to allocate contiguous area.");
		        hLog.LogState = LOG_ERROR_OPEN;
		    	return SD_F_ERROR;
		    }

		    /* seems sometimes file not being expanded or opened
		     * but not causing a error...  why?
		     * see if can check file stat while file is open
		     */
		    fr = f_expand(&SDFile, 5242880, 1); //10240 sectors

		    fr = f_sync(&SDFile);
		    fr = f_stat(fname, &fno);/* Get file status */
		    if(fno.fsize==0){
		    	f_close(&SDFile);
				//die("Failed to allocate contiguous area.");
				hLog.LogState = LOG_ERROR_OPEN;
				return SD_F_ERROR;
		    }

		    //do again
		    fr = f_expand(&SDFile, 5242880, 1); //10240 sector

		    /* Get physical location of the file data */
		    hLog.drv = SDFile.obj.fs->drv;  //was pdrv
		    hLog.lba = SDFile.obj.fs->database + SDFile.obj.fs->csize * (SDFile.obj.sclust - 2);
		    hLog.sectorsRemain=10240; //20480;
		    hLog.file = &SDFile;
			hLog.buffCursorLocation=0;  //reset the place to store data
			hLog.buffActive=0; 			//reset place to store data
		    hLog.LogState = LOG_ACTIVE;
		    hLog.logStartTime = HAL_GetTick();
		    hLog.err=0;

		    HAL_StatusTypeDef result;
#ifdef USE_MEMS
		    //start the DMA for MEMS
		    result = HAL_UART_Receive_DMA(&huart3, (uint8_t*)&RX_data, RX_size);
		    if (result != HAL_OK) {
		        f_close(&SDFile);
		        hLog.LogState = LOG_ERROR_DMA;
		    	return HAL_ERROR; //this will be confusing as FR_OK but failed DMA
		    }
			/* Disable Half Transfer Interrupt */
			__HAL_DMA_DISABLE_IT(huart3.hdmarx, DMA_IT_HT);
			/* Enable UART idle */
			__HAL_UART_ENABLE_IT(&huart3, UART_IT_IDLE);
#endif
			//start DMA for GPS
			result = User_Start_DMA(&huart1);
			if (result != HAL_OK) {
				msg.Len = sprintf(msg.Data,"ERROR: huart1 no idle\n");
				User_Common_SendData(msg);
				HAL_UART_DMAStop(&huart1); //stop the dma becuase could be back packets
			}

			//start DMA for MEMS
			result = User_Start_DMA(&huart3);
			if (result != HAL_OK) {
				msg.Len = sprintf(msg.Data,"ERROR: huart3 no idle\n");
				User_Common_SendData(msg);
				HAL_UART_DMAStop(&huart3);  //stop the dma becuase could be back packets
			}

#ifdef USE_IDLE
			result = HAL_UART_Receive_DMA(&huart1, (uint8_t*)&RxUart1, RX_UART1_SIZE);
			if (result != HAL_OK) {
				f_close(&SDFile);
				hLog.LogState = LOG_ERROR_DMA;
				return HAL_ERROR; //this will be confusing as FR_OK but failed DMA
			}
			/* Disable Half Transfer Interrupt */
			__HAL_DMA_DISABLE_IT(huart1.hdmarx, DMA_IT_HT);
			/* Enable UART idle */
			__HAL_UART_ENABLE_IT(&huart1, UART_IT_IDLE);

#endif

			//start the light flashing (for status)
			User_Set_LED_State(LD2_ON);
			break;

		case LOG_ACTIVE:
			//do nothing
			//handled in UART_RX complete
			break;

		case LOG_STOP:
			//HAL_UART_AbortReceive(&huart3);
			__HAL_UART_DISABLE_IT(&huart1, UART_IT_IDLE);
			HAL_UART_AbortReceive(&huart1);
			HAL_UART_AbortReceive(&huart3);

			fr = f_close(&SDFile);

			//unmount the drive
			f_mount(NULL, (TCHAR const*)SDPath, 0);
			//turn off SD
			//HAL_GPIO_WritePin(GPIOC,GPIO_PIN_6,GPIO_PIN_RESET);
			HAL_SD_DeInit(&hsd1);

			hLog.LogState = LOG_OFF;
			User_Set_LED_State(LD2_FLASH_3);
			break;

		case LOG_OFF:
			//do nothing
			break;

		default:
			/*flash the led*/
			hLog.err=1;
			break;
	}

	return sdr;
}

void HAL_UART_ErrorCallback(UART_HandleTypeDef *huart){
	 User_Set_LED_State(LD2_FLASH_3);
	 //HAL_UART_ERROR_ORE
}




void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart){

    HAL_GPIO_TogglePin(GPIOA, GPIO_PIN_12);

	uint16_t bytesToWrite = huart->RxXferSize - huart->hdmarx->Instance->CNDTR; //  __HAL_DMA_GET_COUNTER(huart->hdmarx);
	uint16_t cursor = 0; //uart rx buffer cursor

	while (bytesToWrite > 0 && hLog.LogState == LOG_ACTIVE) {
		if (512 - hLog.buffCursorLocation > bytesToWrite){
			memcpy(&hLog.buffLogger[hLog.buffActive][hLog.buffCursorLocation],&huart->pRxBuffPtr[cursor],(size_t)bytesToWrite);
			hLog.buffCursorLocation +=bytesToWrite;
			bytesToWrite = 0;
		} else {
			uint16_t bw = bytesToWrite;
			for (; cursor < bw; cursor++) {
				if(hLog.buffCursorLocation>=512){
					if(hLog.buffWriteStatus == BUFF_FULL) {
						hLog.buffWriteStatus = BUFF_OVERRUN;
						hLog.LogState = LOG_ERROR;
						break;
					}
					hLog.buffWriteStatus = BUFF_FULL;
					hLog.buffActive =! hLog.buffActive;
					hLog.buffCursorLocation = 0;
					break;
				}
				/*put the data into the buffer */
				hLog.buffLogger[hLog.buffActive][hLog.buffCursorLocation] = huart->pRxBuffPtr[cursor];
				/* Increment the counters*/
				bytesToWrite --;
				hLog.buffCursorLocation++;
			}
		}
	}; //need to check that buffer full trigger exit
    HAL_GPIO_TogglePin(GPIOA, GPIO_PIN_12);

	//restart the dma
	HAL_UART_Receive_DMA(huart, huart->pRxBuffPtr, (uint32_t)huart->RxXferSize);

}



#ifdef USE_OLD
void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart){

	/*write the DMA data to temp buffer*/
	if(buffData.bytesToWrite<340 && !buffData.busy){
		//buffer empty so cao use it
		uint16_t newBytes = huart->RxXferSize - huart->hdmarx->Instance->CNDTR; //  __HAL_DMA_GET_COUNTER(huart->hdmarx);
		memcpy(buffData.data+buffData.bytesToWrite,huart->pRxBuffPtr,newBytes);
		buffData.bytesToWrite += newBytes;
	} else {//release and return?  maybe read from mainloop
	    HAL_GPIO_TogglePin(GPIOA, GPIO_PIN_12);//,GPIO_PIN_SET);
	}

	//restart the dma
	HAL_UART_Receive_DMA(huart, huart->pRxBuffPtr, (uint32_t)huart->RxXferSize);
	/* split so can restart DMA quick*/

#ifdef USE_DEBUG
	if(!newBytes){
		msg.Len = sprintf(msg.Data,"%li : %i\n",  HAL_GetTick(),buffData.bytesToWrite );
	}else{
		msg.Len = sprintf(msg.Data,"%li : %s\n",  HAL_GetTick(), "not copied" );
	}
	User_Common_SendData(msg);
#endif

//since pack is done here the above buffer is no used.
	//to use it need to add locks, to stop the interupt existing a pack and
	//changing the global variables.
	//User_Buffer_Pack();
}
#endif


/*start the UartRX...
 * find idle then start the DMA...
 */
HAL_StatusTypeDef  User_Start_DMA(UART_HandleTypeDef *huart){

	HAL_StatusTypeDef result = HAL_OK;
	uint16_t packetSize =0;
	uint16_t dmaRxSize  =0;
	uint8_t* dmaBuffer;

	if(huart->Instance==USART3){
		packetSize = MEM_PACKET_SIZE;
		dmaRxSize = RX_SIZE;
		dmaBuffer = (uint8_t*)RX_data;
	}


	if(huart->Instance==USART1){
		packetSize = GPS_PACKET_SIZE;
		dmaRxSize = RX_UART1_SIZE;
		dmaBuffer = (uint8_t*)RxUart1;
	}

#define USE_DMA_NOT_IT

	uint32_t timeStart = HAL_GetTick();
	uint8_t timeout = 0;
	uint8_t keepGoing = 1;

	do{
#ifdef USE_DMA_NOT_IT
		/*start the DMA*/
		result = HAL_UART_Receive_DMA(huart, dmaBuffer, dmaRxSize);
		/* Disable Half Transfer Interrupt */
		__HAL_DMA_DISABLE_IT(huart->hdmarx, DMA_IT_HT);
#else
	//instead of DMA
		result = HAL_UART_Receive_IT(&huart1, dmaBuffer, dmaRxSize);

#endif
		__HAL_UART_CLEAR_FLAG(huart,USART_ISR_IDLE);
		/*poll for IDLE FLAG, then stop and restart the DMA*/
		while(!__HAL_UART_GET_FLAG(huart,USART_ISR_IDLE)){
			if(5000 < HAL_GetTick()-timeStart){
				return HAL_TIMEOUT;
			}
		}
		/*Got an IDLE so check the size*/
		if(huart->RxXferSize - huart->hdmarx->Instance->CNDTR == packetSize ){
			keepGoing = 0;
		}
		result = HAL_UART_AbortReceive(huart);

		if(5000 < HAL_GetTick()-timeStart){
			timeout = 1; //give in five seconds to sync (to allow for GPS)
		}
	} while(keepGoing && !timeout);

	if(timeout)
		return HAL_TIMEOUT; //not really HAL but suits

	/* in an idle before the next packet so lets get started*/
	result = HAL_UART_Receive_DMA(huart, dmaBuffer, packetSize);

	return result;
}


/*takes data and packs into 512byte buffer for writing to sd*/
buffStatus User_Buffer_Pack(void){
	buffStatus bs = hLog.buffWriteStatus;

	//no need to be here if not logging
	if(hLog.LogState != LOG_ACTIVE ) return bs;

#ifdef USE_DEBUG
	msg.Len = sprintf(msg.Data,"%s\n",  "here_pack");
	User_Common_SendData(msg);
#endif

	/* variable to use to shift the write starting point (when needed)*/
	volatile uint16_t cursor =0;


	buffData.busy=1;  //having trouble with overwriting so attempting to lock the buffer to see if is the cause

	/* Fill (pack) the sd buffer with bytes from the buffData that populated by UartRx.
	 * Using memcpy if the active buffer has the space, otherwise loop over bytes to fill the 512 bytes
	 */
	do{
		if (512 - hLog.buffCursorLocation >  buffData.bytesToWrite ){
			//all data will fit
			//pointer the next write location
			memcpy(&hLog.buffLogger[hLog.buffActive][hLog.buffCursorLocation],&buffData.data[cursor],(size_t)buffData.bytesToWrite);
			hLog.buffCursorLocation +=buffData.bytesToWrite;
			buffData.bytesToWrite = 0;
		} else {
			//only some will fit to fill the buffer
			//some efficiency gain here in going back to writing a chuck after new buffer started.
			//the temp var is bit confusing, better to de-increment the bytesToWrite.
			uint16_t bw =buffData.bytesToWrite;
			for (; cursor < bw; cursor++) {
				if(hLog.buffCursorLocation>=512){
					if(hLog.buffWriteStatus == BUFF_FULL) {
						hLog.buffWriteStatus = BUFF_OVERRUN;
						hLog.LogState = LOG_ERROR;
						return BUFF_OVERRUN;
					}
					hLog.buffWriteStatus = BUFF_FULL;
					//Switch buffer and start again
					hLog.buffActive =! hLog.buffActive;
					hLog.buffCursorLocation = 0;
					//go back to the other version -- write a chuck at a time
					break;
				}
				/*put the data into the buffer */
				hLog.buffLogger[hLog.buffActive][hLog.buffCursorLocation] = buffData.data[cursor];
				/* Increment the counters*/
				buffData.bytesToWrite --;
				hLog.buffCursorLocation++;
			}
		}
	}while(buffData.bytesToWrite > 0);

	buffData.busy=0;

	if(hLog.buffWriteStatus == BUFF_FULL){
		User_Buffer_Check(0);
	}

	return bs;
}



/*Checks if buffer full and writes to SD card when needed then swaps the active buffers*/
FRESULT User_Buffer_Check(uint8_t forceWrite){
    FRESULT fr = FR_OK;


    if(hLog.LogState==LOG_OFF && !forceWrite) return fr;

	if(hLog.LogState == LOG_ACTIVE && hLog.buffWriteStatus == BUFF_FULL){
	    /*write the data*/
		uint8_t buff = 1;
		if(hLog.buffActive) buff=0;
	    uint8_t attempts =0;
	    //HAL_GPIO_WritePin(GPIOA, GPIO_PIN_12,GPIO_PIN_SET);

		do{
			fr = disk_write(hLog.drv, (void*)&hLog.buffLogger[buff][0], hLog.lba, 1); //lba no indexing
			if(fr!=FR_OK){
				msg.Len = sprintf(msg.Data, "FR ERROR CODE: %i attempt %i\n", fr, attempts);
				User_Common_SendData(msg);
				if (++attempts >3){
					//manual says FIL is aborted but since I am using low level might be ok.
					hLog.LogState = LOG_ERROR_WRTIE;
					return fr;
				}
			}
		}while(fr!=FR_OK);

		//HAL_GPIO_WritePin(GPIOA, GPIO_PIN_12,GPIO_PIN_RESET);
		hLog.buffWriteStatus = BUFF_READY;
		hLog.lba++;
		if(hLog.sectorsRemain-- == 1)
			hLog.LogState = LOG_STOP;

#ifdef USE_DEBUG
		msg.Len = sprintf(msg.Data,"%s - %i\n",  "hereCheck", hLog.sectorsRemain);
		User_Common_SendData(msg);
#endif
	}

	if(forceWrite && hLog.sectorsRemain >0){
		//Write remaining data to the file
		//need to move to current location first....
		//UINT bw;
		//fr = f_write(hLog.file, &hLog.buffLogger[hLog.buffActive][0], (UINT)hLog.buffCursorLocation, &bw);
	}

	return fr;
}


uint8_t	BSP_PlatformIsDetected(void) {
  uint8_t status = (uint8_t)0x01;
  /* Check SD card detect pin */
  if (HAL_GPIO_ReadPin(GPIOC,GPIO_PIN_9) == GPIO_PIN_RESET) {
    status = (uint8_t)0x00;
  }
  /* USER CODE BEGIN 1 */
  /* user code can be inserted here */
  status = !status;

  /* USER CODE END 1 */
  return status;
}

/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @param  file: The file name as string.
  * @param  line: The line in file as a number.
  * @retval None
  */
void _Error_Handler(char *file, int line)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
  while(1) 
  {
  }
  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t* file, uint32_t line)
{ 
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
    ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/**
  * @}
  */

/**
  * @}
  */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
